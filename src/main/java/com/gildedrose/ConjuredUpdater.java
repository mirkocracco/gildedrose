package com.gildedrose;

public
class ConjuredUpdater
		implements ItemUpdater {

	public
	ConjuredUpdater() {
	}

	@Override
	public
	boolean canIApply(Item item) {
		return item.name.equals("Conjured");
	}

	public
	void update(Item item) {
		item.quality = item.quality - 2;

		item.sellIn = item.sellIn - 1;

		if (item.sellIn < 0) {
			item.quality = item.quality - 2;
		}
	}
}