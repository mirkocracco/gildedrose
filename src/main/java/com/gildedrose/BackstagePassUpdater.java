package com.gildedrose;

public
class BackstagePassUpdater
		implements ItemUpdater {

	public
	BackstagePassUpdater() {
	}

	@Override
	public
	boolean canIApply(Item item) {
		return item.name.equals("Backstage passes to a TAFKAL80ETC concert");
	}

	@Override
	public
	void update(Item item) {
		if (item.quality < 50) {
			item.quality = item.quality + 1;

			if (item.sellIn < 11) {
				item.quality = item.quality + 1;
			}

			if (item.sellIn < 6) {
				item.quality = item.quality + 1;
			}
		}

		item.sellIn = item.sellIn - 1;

		if (item.sellIn < 0) {
			item.quality = 0;
		}
	}
}