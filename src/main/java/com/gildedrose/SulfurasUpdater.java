package com.gildedrose;

public
class SulfurasUpdater
		implements ItemUpdater {

	public
	SulfurasUpdater() {
	}

	@Override
	public
	boolean canIApply(Item item) {
		return item.name.equals("Sulfuras, Hand of Ragnaros");
	}

	public
	void update(Item item) {
	}
}