package com.gildedrose;

public
class DefaultItemUpdater
		implements ItemUpdater {

	public
	DefaultItemUpdater() {
	}

	@Override
	public
	boolean canIApply(Item item) {
		return true;
	}

	public
	void update(Item item) {
		if (item.quality > 0) {
			item.quality = item.quality - 1;
		}

		item.sellIn = item.sellIn - 1;

		if (item.sellIn < 0) {
			if (item.quality > 0) {
				item.quality = item.quality - 1;
			}
		}
	}
}