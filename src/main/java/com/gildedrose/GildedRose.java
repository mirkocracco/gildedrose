package com.gildedrose;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GildedRose {

	Item[] items;

	public
	GildedRose(Item[] items) {
		this.items = items;
	}

	public
	void updateQuality() {

		List<ItemUpdater> updaters = Arrays.asList(new SulfurasUpdater(),
		                                           new AgedBrieUpdater(),
		                                           new BackstagePassUpdater(),
		                                           new ConjuredUpdater(),
		                                           new DefaultItemUpdater());

		for (Item item : items) {

			ItemUpdater updater = updaters.stream()
			                              .filter(itemUpdater -> itemUpdater.canIApply(item))
			                              .findFirst()
			                              .get();
			updater.update(item);
		}
	}

}
