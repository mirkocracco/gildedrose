package com.gildedrose;

public
interface ItemUpdater {
	boolean canIApply(Item item);
	void update(Item item);
}
