package com.gildedrose;

public
class AgedBrieUpdater
		implements ItemUpdater {
	public
	AgedBrieUpdater() {
	}

	@Override
	public
	boolean canIApply(Item item) {
		return item.name.equals("Aged Brie");
	}

	public
	void update(Item item) {
		if (item.quality < 50) {
			item.quality = item.quality + 1;
		}

		item.sellIn = item.sellIn - 1;

		if (item.sellIn < 0) {
			if (item.quality < 50) {
				item.quality = item.quality + 1;
			}
		}
	}
}