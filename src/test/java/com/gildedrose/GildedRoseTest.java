package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

	@Test
	public
	void oggettoGenericoPerdeQualita() {
		Item item = updateQualityForItem("foo", 1, 1);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void oggettoPerdeQualitaMaNonSottoZero() {
		Item item = updateQualityForItem("foo", 1, 0);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void oggettoPerdeQualitaMaNonSottoZeroSeSellinNegativo() {
		Item item = updateQualityForItem("foo", -1, 0);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void oggettoGenericoPerdeQualitaDoppiaSeScaduto() {
		Item item = updateQualityForItem("foo", 0, 2);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void sulfurasNonPerdeQualita() {
		Item item = updateQualityForItem("Sulfuras, Hand of Ragnaros", 1, 80);
		assertEquals(80, item.quality);
	}

	@Test
	public
	void sulfurasNonPerdeSellIn() {
		Item item = updateQualityForItem("Sulfuras, Hand of Ragnaros", 1, 80);
		assertEquals(1, item.sellIn);
	}

	@Test
	public
	void sulfurasNonPerdeQualitaSeSellinNegativa() {
		Item item = updateQualityForItem("Sulfuras, Hand of Ragnaros", -1, 80);
		assertEquals(80, item.quality);
	}

	@Test
	public
	void agedBrieNonPerdeQualita() {
		Item item = updateQualityForItem("Aged Brie", 5, 2);
		assertEquals(3, item.quality);
	}

	@Test
	public
	void agedBrieAumentaQualitaSeSellinMinoreUgualeZero() {
		Item item = updateQualityForItem("Aged Brie", 0, 2);
		assertEquals(4, item.quality);
		Item item2 = updateQualityForItem("Aged Brie", -1, 2);
		assertEquals(4, item2.quality);
	}

	@Test
	public
	void agedBrieNonAumentaQualitaOltre50() {
		Item item = updateQualityForItem("Aged Brie", 0, 50);
		assertEquals(50, item.quality);
	}

	@Test
	public
	void sellInGenericaCala() {
		Item item = updateQualityForItem("Prodotto", 1, 2);
		assertEquals(0, item.sellIn);
	}

	@Test
	public
	void bigliettoSellInMAggioreDieci() {
		Item item = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", 11, 2);
		assertEquals(3, item.quality);
	}

	@Test
	public
	void bigliettoSellInTra6e10() {
		Item item = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", 10, 2);
		assertEquals(4, item.quality);
	}

	@Test
	public
	void bigliettoSellInTra1e5() {
		Item item = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", 5, 2);
		assertEquals(5, item.quality);
	}

	@Test
	public
	void bigliettoSellInMinoreUguale0() {
		Item item1 = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", 0, 2);
		assertEquals(0, item1.quality);

		Item item2 = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", -1, 2);
		assertEquals(0, item2.quality);
	}

	@Test
	public
	void bigliettoQualitaMaggiore50() {
		Item item = updateQualityForItem("Backstage passes to a TAFKAL80ETC concert", 10, 50);
		assertEquals(50, item.quality);
	}

	@Test
	public
	void conjuredDegradaAVelocitaDoppia() {
		Item item = updateQualityForItem("Conjured", 10, 2);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void conjuredDegradaAVelocitaQuadruplaUnaVoltaScaduto() {
		Item item = updateQualityForItem("Conjured", -1, 4);
		assertEquals(0, item.quality);
	}

	@Test
	public
	void sellInConjuredCala() {
		Item item = updateQualityForItem("Conjured", 1, 2);
		assertEquals(0, item.sellIn);
	}

	private
	Item updateQualityForItem(String name, int sellIn, int quality) {
		Item[] items = new Item[]{new Item(name, sellIn, quality)};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		return app.items[0];
	}

}